from lxml import etree as ET
from telebot.types import Message
import time
import random

class roomCreation:
    name = str
    id = int
    USERS = set()
    time = int

    def write_to_doc(self, id, USERS, name, time = 0):
        root = ET.Element('rooms')
        level1 = ET.SubElement(root, str(name))
        rid= ET.SubElement(level1, 'ID')
        rid.text = str(id)
        rname = ET.SubElement(level1, 'name')
        rname.text = name
        users = ET.SubElement(level1, 'users')
        users.text = str(USERS)
        rtime = ET.SubElement(level1, 'tim1')
        rtime.text = str(time)

        tree = ET.ElementTree(root)
        tree.write(f'{name}.xml', pretty_print=True, xml_declaration=True,   encoding="utf-8")

    def __init__(self, message: Message):
        try:
            info = message.text.split(' ')
            if len(info) > 1:
                self.name = info[1]
            else:
                self.name = 'someName'
            for i in range(2, len(info)):
                self.USERS.add(info[i])
            self.id = (int)(str(time.time()).split('.')[0] + str(random.randint(1,999)))

            self.write_to_doc(self.id, self.USERS,self.name)

        except:
            print("Виникла помилка при вводі")
            return

    def adding_to_xml(self, subelement):
        time_info = ET.SubElement(subelement, 'meeting_time')
        time_info.text = '18:00'

    def editing_xml(self, obj: ET, text):
        obj.text = text

    def detect_users(self):  #detection of usersmentioned in room
        pass

    def __str__(self):
        return str(self.id)

 #self needed